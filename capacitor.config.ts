import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'exam-template',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;
