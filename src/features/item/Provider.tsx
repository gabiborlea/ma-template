import React, { useCallback, useEffect, useReducer } from "react";
import { getLogger } from "../base";
import { getItems, updateItem } from "./api";
import { ItemProps } from "./ItemProps";
import PropTypes from "prop-types";
import { AxiosError } from "axios";

type SaveFunction = (item: ItemProps) => Promise<any>;
const log = getLogger("Provider");

export interface State {
  items?: ItemProps[];
  fetching: boolean;
  fetchingError?: Error | null;
  saving: boolean;
  savingError?: Error | null;
  saveFunction?: SaveFunction;
  text?: string;
  setText?: Function;
}

interface ActionProps {
  type: string;
  payload?: any;
}

const initialState: State = {
  fetching: false,
  saving: false,
};

const FETCH_ITEMS_STARTED = "FETCH_ITEMS_STARTED";
const FETCH_ITEMS_SUCCEEDED = "FETCH_ITEMS_SUCCEEDED";
const FETCH_ITEMS_FAILED = "FETCH_ITEMS_FAILED";
const SAVE_ITEM_STARTED = "SAVE_ITEM_STARTED";
const SAVE_ITEM_SUCCEEDED = "SAVE_ITEM_SUCCEEDED";
const SAVE_ITEM_FAILED = "SAVE_ITEM_FAILED";
const SET_TEXT = "SET_TEXT";

const reducer: (state: State, action: ActionProps) => State = (
  state,
  { type, payload }
) => {
  switch (type) {
    case FETCH_ITEMS_STARTED:
      return { ...state, fetching: true, fetchingError: null };
    case FETCH_ITEMS_SUCCEEDED:
      return { ...state, items: payload.items, fetching: false };
    case FETCH_ITEMS_FAILED:
      return { ...state, fetchingError: payload.error, fetching: false };
    case SAVE_ITEM_STARTED:
      return { ...state, savingError: null, saving: true };
    case SAVE_ITEM_SUCCEEDED:
      const items = [...(state.items || [])];
      const item = payload.item;
      const index = items.findIndex((it) => it.id === item.id);
      if (index === -1) {
        items.splice(0, 0, item);
      } else {
        items[index] = item;
      }
      return { ...state, items, saving: false };
    case SAVE_ITEM_FAILED:
      return { ...state, savingError: payload.error, saving: false };
    case SET_TEXT:
      return { ...state, text: payload.text };
    default:
      return state;
  }
};

export const Context = React.createContext<State>(initialState);

interface ProviderProps {
  children: PropTypes.ReactNodeLike;
}

export const Provider: React.FC<ProviderProps> = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const { items, fetching, fetchingError, saving, savingError } = state;

  const saveFunction = useCallback<SaveFunction>(saveItemCallback, []);

  const value = {
    items,
    fetching,
    fetchingError,
    saving,
    savingError,
    saveFunction,
    setText,
  };

  useEffect(getItemsEffect, []);

  log("returns");
  return <Context.Provider value={value}>{children}</Context.Provider>;

  function getItemsEffect() {
    let canceled = false;
    fetchItems();
    return () => {
      canceled = true;
    };

    async function fetchItems() {
      try {
        log("fetchItems started");
        dispatch({ type: FETCH_ITEMS_STARTED });
        let items;
        items = await getItems();
        log("fetchItems succeeded");
        if (!canceled) {
          dispatch({ type: FETCH_ITEMS_SUCCEEDED, payload: { items } });
        }
      } catch (error) {
        log("fetchItems failed");
        dispatch({ type: FETCH_ITEMS_FAILED, payload: { error } });
      }
    }
  }

  async function saveItemCallback(item: ItemProps) {
    try {
      log("saveItem started");
      dispatch({ type: SAVE_ITEM_STARTED });
      const savedItem = await updateItem(item);
      log("saveItem succeeded");
      dispatch({
        type: SAVE_ITEM_SUCCEEDED,
        payload: { item: savedItem },
      });
    } catch (error) {
      log("saveItem failed");
      dispatch({ type: SAVE_ITEM_FAILED, payload: { error } });
      if ((error as AxiosError)?.response?.status === 409) {
        getItemsEffect();
      }
    }
  }

  function setText(text: string) {
    dispatch({ type: SET_TEXT, payload: { text } });
  }
};
