import { IonList, IonLoading } from "@ionic/react";
import { useContext } from "react";
import { ItemProps } from "../ItemProps";
import { Context } from "../Provider";
import Item from "./Item";

const ItemList: React.FC = () => {
  const { items, fetching, fetchingError, saving, savingError, saveFunction } = useContext(Context);
  const onClick = (item: ItemProps) => {
    saveFunction && saveFunction(item);
  };
  return (
    <>
      <IonLoading isOpen={fetching} message="Fetching" />
      {fetchingError && (
        <div>{fetchingError.message || "Failed to fetch"}</div>
      )}
      <IonLoading isOpen={saving} message="Saving" />
      {savingError && (
        <div>{savingError.message || "Failed to save"}</div>
      )}
      {items && (
        <IonList>
          {items.map((props: ItemProps) => (
            <Item
              key={props.id}
              id={props.id}
              onEdit={onClick}
            />
          ))}
        </IonList>
      )}
    </>
  );
};

export default ItemList;
