import { IonInput } from "@ionic/react";
import { useContext } from "react";
import { Context } from "../Provider";

const Input: React.FC = () => {
  const { text, setText } = useContext(Context);
  return (
    <IonInput
      value={text}
      placeholder="Enter Input"
      onIonChange={(e) => setText && setText(e.detail.value!)}
    ></IonInput>
  );
};

export default Input;