import { IonItem, IonLabel } from "@ionic/react";
import { ItemProps } from "../ItemProps";

interface ItemPropsExtended extends ItemProps {
  onEdit: (item: ItemProps) => void;
}

const Item: React.FC<ItemPropsExtended> = (props: ItemPropsExtended) => (
  <IonItem>
    <IonLabel>{props.id}</IonLabel>
  </IonItem>
);

export default Item;
