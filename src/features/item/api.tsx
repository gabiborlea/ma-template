import axios from "axios";
import { baseUrl, getLogger, withLogs } from "../base";
import { ItemProps } from "./ItemProps";

const itemUrl = `http://${baseUrl}/task`;

export const getItems: () => Promise<ItemProps[]> = () => {
  return withLogs(axios.get(itemUrl), "getItems");
};

export const getFilteredItems: (filter: string) => Promise<ItemProps[]> = (filter) => {
    return withLogs(axios.get(`${itemUrl}?q=${filter}`), "getFilteredItems");
}

export const createItem: (item: ItemProps) => Promise<ItemProps[]> = (item) => {
  return withLogs(axios.post(itemUrl, item), "createItem");
};

export const updateItem: (item: ItemProps) => Promise<ItemProps[]> = (item) => {
  return withLogs(axios.put(`${itemUrl}/${item.id}`, item), "updateItem");
};

export const deleteItem: (item: ItemProps) => Promise<ItemProps[]> = (item) => {
    return withLogs(axios.put(`${itemUrl}/${item.id}`, item), "updateItem");
  };

interface MessageData {
  type: string;
  payload: ItemProps;
}

const log = getLogger("ws");

export const newWebSocket = (
  token: string,
  onMessage: (data: MessageData) => void
) => {
  const ws = new WebSocket(`ws://${baseUrl}`);
  ws.onopen = () => {
    log("web socket onopen");
    ws.send(JSON.stringify({ type: "authorization", payload: { token } }));
  };
  ws.onclose = () => {
    log("web socket onclose");
  };
  ws.onerror = (error) => {
    log("web socket onerror", error);
  };
  ws.onmessage = (messageEvent) => {
    log("web socket onmessage");
    onMessage(JSON.parse(messageEvent.data));
  };
  return () => {
    ws.close();
  };
};
