import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import Input from '../features/item/components/Input';
import List from '../features/item/components/List';
import './Home.css';

const Home: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Blank</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Blank</IonTitle>
          </IonToolbar>
        </IonHeader>
        <Input />
        <List />
      </IonContent>
    </IonPage>
  );
};

export default Home;
